const fs = require('fs');
const csv = require('csv-parser');
const result = [0, 0, 0, 0, 0];
const years = {}; 
const priBuiReg = {};
const princAct = {};
let yearss = [];

fs.createReadStream('../csv_data/company_master_Maharashtra.csv')
  .pipe(csv())
  .on('data', (datas) => {
      //Problem1_Authorized_Cap
      if (datas.AUTHORIZED_CAPITAL < 1e5){
        result[0]+=1;
      }    
      if (datas.AUTHORIZED_CAPITAL >= 1e5 && datas.AUTHORIZED_CAPITAL < 1e6){
        result[1]+=1;
      } 
      if (datas.AUTHORIZED_CAPITAL >= 1e6 && datas.AUTHORIZED_CAPITAL < 1e7){
        result[2]+=1;
      }    
      if (datas.AUTHORIZED_CAPITAL >= 1e8 && datas.AUTHORIZED_CAPITAL < 1e9){
        result[3]+=1;
      }    
      if (datas.AUTHORIZED_CAPITAL > 1e9){
        result[4]+=1;
      }    
     
     //Problem2_DATE_OF_REGISTRATION
   const dates = datas.DATE_OF_REGISTRATION.split('-');
   new_date = new Date(`${dates[0]}/${dates[1]}/${dates[2]}`);
   if(dates[2]>2000 && dates[2]<=2015){
    if (!years[dates[2]]) {
      years[dates[2]] = 1;
    } else {
      years[dates[2]] += 1;
    }
  }
 //Problem3_PRI_BUSI_ACTIVITY
 if(dates[2]== 2015){
   if(!priBuiReg[datas.PRINCIPAL_BUSINESS_ACTIVITY]){
    priBuiReg[datas.PRINCIPAL_BUSINESS_ACTIVITY] = 1;
    } else{
      priBuiReg[datas.PRINCIPAL_BUSINESS_ACTIVITY] +=1;
   }
 }


 //Problem4_DATE_OF_REGISTRATION and PRI_BUSI_ACTIVITY
   const array=datas.PRINCIPAL_BUSINESS_ACTIVITY;
   if(dates[2]>= 2010 && dates[2]<= 2015){
     if(!princAct[array]){
       princAct[array] = {};
       yearss = princAct[array];
       if(!yearss[dates[2]]){
         yearss[dates[2]] = 1;
       }
     } else {
       yearss =princAct[array]; 
       if(!yearss[dates[2]]){
         yearss[dates[2]] = 1;
       } else {
         yearss[dates[2]] += 1;
       }
       }
     }
   
  })
.on('end', () => {
    fs.writeFile('Auth_cap_graph1.json', JSON.stringify(result), () => {});
    fs.writeFile('Date_reg_graph2.json', JSON.stringify(years), () => {});
    fs.writeFile('pri_bui_graph3.json', JSON.stringify(priBuiReg), () => {});
    fs.writeFile('stack_bar_graph4.json', JSON.stringify(princAct), () => {});
    });