fetch('./pri_bui_graph3.json')
  .then(resp => resp.json())
  .then((data) => {
    const sorted_key = Object.keys(data).sort((a, b) => data[b] - data[a]);
    const key = [];
    const no_of_reg = [];
    for (let i = 0 ; i < 10 ; i++) {
      key.push(sorted_key[i]);
      no_of_reg.push(data[sorted_key[i]]);
    }
    Highcharts.chart('container3', {
      chart:{
        type:'column',
      },
      title:{
        text:'Top registrations by "Principal Business Activity" for the year 2015',
      },
      xAxis:{
        categories:key,
      },
      yAxis:{
        title:{
          text:'No of company registrations',
        },
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      series:[{
        name:'Number of Company Registrations',
          data:no_of_reg,
      }],
    });
  });
