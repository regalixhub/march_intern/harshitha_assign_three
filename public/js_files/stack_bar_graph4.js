fetch('./stack_bar_graph4.json')
  .then(resp => resp.json())
  .then((result) =>{
    const year = [2010];
    for (let i = 0 ; year[i] <= 2015 ; i++){
      year.push(1+year[i]);
    }
    const objects = [];
    const key1 = Object.keys(result);
    for (let i = 0 ; i < key1.length ; i++){
      objects.push({
        name:key1[i],
        data:Object.values(result[key1[i]]),
      },);
    }
    Highcharts.chart('container4', {
      chart:{
        type:'column'
      },
      title:{
        text:'STACKED_BAR_CHART'
      },
      xAxis:{
        categories:year,
        title:{
          text:'YEAR',
        }
      },
      yAxis:{
        title:{
          text:'NO_PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_YEAR'
        }
      },
      plotOptions:{
        series:{
          stacking:'normal',
        },
      },
      series:objects,
    });
  });
