 fetch('./Auth_cap_graph1.json')
    .then(function(response) {
        response.json().then(function(data) {
     Highcharts.chart('container1', {
         chart: {
             type: 'column'
         },
         title: {
             text: 'AUTHORIZED_CAPITAL'
         },
         xAxis: {
             categories: ['<1L', '1L to 10L', '10L to 1Cr', '10Cr to 100Cr', 'More than 100Cr']
         },
         yAxis: {
             title: {
             text: "Authorized_Capital in Rupees"
            }
         },
         plotOptions: {
            column: {
              dataLabels: {
                enabled: true,
              },
            },
          },
         series: [{
             name:'Authorized_Capital',
             data:data,
                 }]
             });
         });
        });































        