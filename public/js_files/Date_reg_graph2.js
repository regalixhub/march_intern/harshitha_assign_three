fetch("./Date_reg_graph2.json")
  .then((resp) => resp.json())
  .then(function (data) {
    const key = Object.keys(data);
    const vs = Object.values(data);
    Highcharts.chart('container2', {
      chart:{
        type:'column'
      },
      title:{
        text:'NO_OF_COMPANY_REGISTERED_BY_YEAR'
      },
      xAxis:{
        categories:key,
        text:'YEAR'
      },
      yAxis:{
        min:0,
        title:{
          text:'NO_OF_COMPANY_REGISTERED'
        }
      },
      legend: {
        reversed:true,
      },
      plotOptions:{
            column: {
              dataLabels: {
                enabled: true,
              },
            },
        },
      series:[{
        name:'COMPANY_REGISTERED_YEAR',
        data:vs,
      }],
    });
  });
